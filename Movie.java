import java.util.Scanner;
public class Movie{
	Scanner reader = new Scanner(System.in);
	private String title;
	private int releaseYear;
	private String producer;
	
	public String getTitle(){
		return this.title;
	}
	
	public String getProducer(){
		return this.producer;
	}
	
	public int getReleaseYear(){
		return this.releaseYear;
	}
	
	public void setTitle(String newTitle){
		this.title = newTitle;
		
	}
	
	public void setProducer(String newProducer){
		this.producer = newProducer;
		
	}
	
	public void setReleaseYear(int newReleaseYear){
		this.releaseYear = newReleaseYear;
		
	}
	
	public Movie(String title, String producer, int releaseYear){
		this.title = title;
		this.producer = producer;
		this.releaseYear = releaseYear;
	}
	
	
	public void printTitle(String x){
		System.out.println(x);
	}
}
