import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Movie[] movies = new Movie[4];

		
			
		for(int i = 0; i < 4; i++){
			
			//setting the movies values
			System.out.println("What is the title, producer name and release year of your movie?");
			
			movies[i] = new Movie(reader.next(), reader.next(), reader.nextInt());
			
			System.out.println();
			System.out.println();
		}
		
		//printing the values of the last movie
		System.out.println();
		System.out.println();
		System.out.println(movies[3].getTitle());
		System.out.println(movies[3].getProducer());
		System.out.println(movies[3].getReleaseYear());
		System.out.println();
		System.out.println();
		
		//re assigning the new values to the last movie
		System.out.println("What is the title of the new last movie");
		movies[3].setTitle(reader.next());
		System.out.println("what is the producer of the new last movie");
		movies[3].setProducer(reader.next());
		System.out.println("what is the release year of the new last movie");
		movies[3].setReleaseYear(reader.nextInt());
		
		//printing the new values of the last movie
		System.out.println();
		System.out.println();
		System.out.println(movies[3].getTitle());
		System.out.println(movies[3].getProducer());
		System.out.println(movies[3].getReleaseYear());
		System.out.println();
		System.out.println();
		
		movies[3].printTitle(movies[3].getTitle());
	}
}
